//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by Pascal Hintze on 21.10.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Spain", "UK", "Ukraine", "US"].shuffled()
    @State private var correctAnswer = Int.random(in: 0...2)
    
    @State private var showingScore = false
    @State private var scoreTitle = ""
    @State private var score = 0
    @State private var tappedFlag = ""
    @State private var questionCounter = 0
    @State private var showEndResult = false
    
    @State private var answer: Int? = nil

    let labels = [
        "Estonia": "Flag with three horizontal stripes. Top stripe blue, middle stripe black, bottom stripe white.",
        "France": "Flag with three vertical stripes. Left stripe blue, middle stripe white, right stripe red.",
        "Germany": "Flag with three horizontal stripes. Top stripe black, middle stripe red, bottom stripe gold.",
        "Ireland": "Flag with three vertical stripes. Left stripe green, middle stripe white, right stripe orange.",
        "Italy": "Flag with three vertical stripes. Left stripe green, middle stripe white, right stripe red.",
        "Nigeria": "Flag with three vertical stripes. Left stripe green, middle stripe white, right stripe green.",
        "Poland": "Flag with two horizontal stripes. Top stripe white, bottom stripe red.",
        "Spain": "Flag with three horizontal stripes. Top thin stripe red, middle thick stripe gold with a crest on the left, bottom thin stripe red.",
        "UK": "Flag with overlapping red and white crosses, both straight and diagonally, on a blue background.",
        "Ukraine": "Flag with two horizontal stripes. Top stripe blue, bottom stripe yellow.",
        "US": "Flag with many red and white stripes, with white stars on a blue background in the top-left corner."
    ]

    
    var body: some View {
        ZStack {
            RadialGradient(stops: [
                .init(color: Color(red: 0.1, green: 0.2, blue: 0.45), location: 0.3),
                .init(color: Color(red: 0.76, green: 0.15, blue: 0.26), location: 0.3)
            ], center: .top, startRadius: 200, endRadius: 700)
            .ignoresSafeArea()
            
            VStack {
                Spacer()
                
                Text("Guess the Flag")
                    .font(.largeTitle.bold())
                    .foregroundStyle(.white)
                VStack(spacing: 15) {
                    VStack {
                        Text("Tap the flag of")
                            .foregroundStyle(.secondary)
                            .font(.subheadline.weight(.heavy))
                        
                        Text(countries[correctAnswer])
                            .font(.largeTitle.weight(.semibold))
                    }
                    
                    ForEach(0..<3) {number in
                        Button {
                            withAnimation {
                                answer = number
                            }
                           flagTapped(number)
                        } label: {
                            FlagView(image: Image(countries[number]))
                        }
                        .rotation3DEffect(.degrees(answer == number ? 360 : .zero), axis: (x: 0.0, y: 1.0, z: 0.0))
                        .opacity(answer != nil && answer != number ? 0.25 : 1)
                        .scaleEffect(answer != nil && answer != number ? 0.75 : 1)
                        .accessibilityLabel(labels[countries[number], default: "Unknown flag"])
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical, 20)
                .background(.regularMaterial)
            .clipShape(.rect(cornerRadius: 20))
            
                
                Spacer()
                Spacer()
                
                Text("Score: \(score)")
                    .foregroundStyle(.white)
                    .font(.title.bold())
                
                Spacer()
            }
            .padding()
        }
        .alert(scoreTitle, isPresented: $showingScore) {
            Button("Continue", action: askQuestion)
        } message: {
            if tappedFlag == "" {
                Text("Your score is \(score)")
            } else {
                Text("Thats the flag of \(tappedFlag) \nYour score is \(score)")
            }
        }
        .alert("End Result", isPresented: $showEndResult) {
            Button("Restart", action: restartGame)
        } message: {
            if score < 3 {
                Text("Your final score is \(score). You can do it better!")
            } else if score > 3 && score < 7 {
                Text("Your final score is \(score). Not bad!")
            } else if score > 7 {
                Text ("Your final score is \(score). You answered everything correct.")
            }
        }
    }
    
    func flagTapped(_ number: Int) {
        if number == correctAnswer {
            scoreTitle = "Correct"
            score += 1
            tappedFlag = ""
            
        } else {
            scoreTitle = "Wrong"
            score -= 1
            tappedFlag = countries[number]
        }
        showingScore = true
    }
    
    func askQuestion() {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        questionCounter += 1
        answer = nil
        
        if questionCounter == 2 {
            showEndResult = true
        }
        
    }
    
    func restartGame() {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        questionCounter = 0
        score = 0
        answer = nil
    }
    
    struct FlagView: View {
        var image: Image    
        
        var body: some View {
            image
                .clipShape(.capsule)
                .shadow(radius: 5)
        }
            
    }
    
}

#Preview {
    ContentView()
}
