# GuessTheFlag

GuessTheFlag is a guessing game that helps users learn some of the many flags of the world.

This game was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/guess-the-flag-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- VStack
- Image
- LinearGradient
- Alerts