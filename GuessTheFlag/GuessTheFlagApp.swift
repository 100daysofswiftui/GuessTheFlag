//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Pascal Hintze on 21.10.2023.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
